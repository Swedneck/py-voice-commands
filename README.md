# Requirements
`pip install --user cffi deepspeech enum34 numpy pvporcupine PyAudio pycparser PySoundFile Wave`

Download [deepspeech model](https://github.com/mozilla/DeepSpeech/releases/download/v0.6.0/deepspeech-0.6.0-models.tar.gz)  
unpack it to /home/$USER/.deepspeech

# Usage
Run `daemon.py --keywords computer`, to listen for the word "computer".
Use the flag `--help` to see a list of available keywords.

Upon hearing the keyword, it will listen for 3 seconds and execute the
recognized command.

# Customization
Edit the if/elif statements in voice_commands.py and restart the daemon.

# Resources used
https://github.com/Picovoice/porcupine  
https://github.com/mozilla/DeepSpeech  
https://freesound.org/people/YourFriendJesse/sounds/235911/  
https://freesound.org/people/distillerystudio/sounds/327736/  
