#!/bin/env python3
import pyaudio
import wave
import client
import os

user = os.environ.get('USER')
deepspeech_dir = f'/home/{user}/.deepspeech'
model = f'{deepspeech_dir}/output_graph.pbmm'
lm = f'{deepspeech_dir}/lm.binary'
lm_alpha = 0.75
lm_beta = 1.85
trie = f'{deepspeech_dir}/trie'
beam_width = 500

CHUNK = 1024
FORMAT = pyaudio.paInt16
CHANNELS = 1
RATE = 16000
RECORD_SECONDS = 2

WAVE_OUTPUT_FILENAME = '/tmp/voice_command.wav'


def metadata_to_string(metadata):
    return ''.join(item.character for item in metadata.items)


def main():
    p = pyaudio.PyAudio()

    stream = p.open(format=FORMAT, channels=CHANNELS, rate=RATE, input=True,
                    frames_per_buffer=CHUNK)

    print("Recording *")

    frames = []

    for i in range(0, int(RATE / CHUNK * RECORD_SECONDS)):
        data = stream.read(CHUNK)
        frames.append(data)

    stream.stop_stream()
    stream.close()
    p.terminate()

    wf = wave.open(WAVE_OUTPUT_FILENAME, 'wb')
    wf.setnchannels(CHANNELS)
    wf.setsampwidth(p.get_sample_size(FORMAT))
    wf.setframerate(RATE)
    wf.writeframes(b''.join(frames))
    wf.close()

    print("Done recording *")

    result = client.main(model=model, lm=lm, trie=trie, audio=WAVE_OUTPUT_FILENAME)

    print(f"Result: {result}")

    if result in ('music pause', 'music poles', 'pause', 'stop', 'poor',
                  'paul'):
        os.system('playerctl pause')
    elif result in ('music play', 'play', 'a'):
        os.system('playerctl play')
    elif result in ('music next', 'music neck', 'new thick neck', 'music not',
                    'music man', 'next song', 'skip track', 'yet try',
                    'get traps', 'not so', 'my son', 'yes trap', 'but so',
                    'miss so', 'yet so', 'yes pro', 'skip', 'next', 'get'):
        os.system('playerctl next')
    elif result in ('music previous', 'previous track', 'previous', 'back'):
        os.system('playerctl previous')
    elif result in ('youtube pause', 'you two pause', 'you do pause',
                    'you to pause', 'to pause'):
        os.system('playerctl -p plasma-browser-integration pause')
    elif result in ('youtube play', 'youtube continue', 'you two come in you',
                    'you two come in', 'you do continue', 'you to continue',
                    'you to play', 'you two play', 'you do play',
                    'you do come in you', 'you to be play', 'to play'):
        os.system('playerctl -p plasma-browser-integration play')
    else:
        os.system('paplay error.wav')
